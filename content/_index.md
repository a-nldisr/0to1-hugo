#

As a quick learner, I embrace new technologies as opportunities to innovate. I specialize in using my experience to help companies by transforming hard issues into byte-sized solutions. With a proven track record of enhancing the performance of both systems and teams, my expertise lies in distilling complex problems into impactful solutions that drive progress. With a proactive mindset, I specialize in pinpointing improvements, crafting a structured approach to a suitable solution, and communicating effectively within the organization. My focus is always on delivering a functional solution.

Within teams, I take ownership, act as a bridge between IT and management, and help teams grow and professionalize.

Starting in 2022, I am available for Freelance and Contractor projects supporting your teams as a Cloud Engineer, SRE, DevOps Engineer, and Platform Engineer. In this capacity I can support your teams in a collaborative role or in a technical leadership role where I help your teams grow and improve their engineering excellence.

In the binary numeral system, there are two values used to generate results; zero and one. My main focus is to add value to your business.

*There are only 10 types of people in the world: Those who understand binary, and those who don’t.*


Rogier Dikkes
