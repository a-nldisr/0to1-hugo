---
Tile: First few steps with a CMS
Subtitle: Messing around with Forestry.io
date: 2020-09-17T22:07:00+00:00
blog_post: ''
tags:
- cms
- hugo
- forestry
Title: CMS experience for a non web developer

---
As a platform engineer you are asked to help out make developers scale their application, how to make their work easier and more secure. However the specifics on how to make this icon's background transparent or how to use a clean and sexy font is something that I have very little experience with.

The not-invented-here syndrome is something that is really big in the HPC landscape, since often solutions are not invented yet. And in alot of other software developing fields people tend to first build something before Googling it. In platform engineering however most of the solutions seem to already be there for the day to day task. So when I decided to make a blog I wanted off the shelf products, something easy to maintain, something fast and quick to integrate into existing technology.

Since I was working more and more in Golang I decided to use Hugo to give that Golang experience a bit of a boost. HTML and CSS however are not really my thing and while I write tons of YAML and Markdown daily I wanted to experience first hand what this thing called CMS was actually about. Would it take away all my HTML editing issues away and could it allow me to release content even quicker? Content creators love a CMS because they no longer need to wait for Developers to put their content live, they can just edit their webpage with a few easy clicks. I was told.

After a quick Google search I found [https://forestry.io](https://forestry.io) as possible candidate, it integrated into Hugo and hooked up into Git. Also a requirement was that it was a headless CMS, I wanted to be able to decouple it from the page instantly if the CMS was not what I expected. I even found Gitlab as import option, so here I am my first blog post with a CMS!

For this domain, where the content is actually 99% blog posts, the integration could not have been easier. I imported the code from Gitlab, clicked a few menus and poof... I had a CMS fully working and ready to go, all within 5 minutes. Blog posts can be edited easy, images adding is no longer a pain. All very fancy! The only thing to investigate is why formatting in the Title is a bit off, however most of it seems to work perfect.

However I started out with another new domain of mine: [https://imkerijsoest.nl](https://imkerijsoest.nl). Integration was not that simple, since most of the content was not blog posts but HTML and CSS. That domain is also a Hugo page, it however has another theme that relies a bit more on HTML and CSS for displaying the content. This theme has most of the content on the front page, my contact details, some information, pictures. I intent not to use the blog post functionality on this domain that often. I just want people who want to buy a jar of honey know where the shops are where I deliver, the blog posts will only be used for giving people tips on how to decrystallize their honey. But I don't expect to blog once a week or even once a month.

## Is it worth adding a CMS?

Now I found out that Forestry really shines in making and editing blog posts in Yaml or Markdown format. As advertised it can create quick static content. But for editing HTML and CSS it seems to lack options. Investigating further the word HTML seems to be used a couple times in the documentation in relation to other subjects, but it is not the main focus. The main focus is generating static content quickly. My daily IDE will likely give more support with HTML and CSS.

Adding a CMS will save me a couple minutes here and there while editing YAML and markdown, however is it ground breaking for me? Not in the current configuration. Perhaps when I port over the main pages to support YAML or markdown blocks, but for me all this time spend porting over my current setup is not worth the effort. 

**Will I stop using the CMS?** No, it saves me a couple minutes here and there.

**Would I be willing to pay for it for hobby projects?** No

**Would I suggest Forestry to others?** Yes and No. I think it works great on the pages where it is intended for, ideal to use with multiple users. The only negative is that commit messages are standardized. It seems to have lots of features for multiple users I haven't touched. For single use however I would not recommend it unless you like playing around with stuff. 

In that aspect Forestry has a great model, it is free up till 3 hobby projects. After that you pay. 