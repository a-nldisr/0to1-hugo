---
title: Setting a correct date on Beautiful Hugo
subtitle: The content writes itself
date: 2020-06-30
tags:
- hugo
- beautifulhugo
- theme
draft: true

---
A while ago someone showed me how Hugo integrates perfectly with Gitlab. He showed me how easy it was to edit the pages, generate content and the benefits you gain while using static content.
This sparked my interest and quickly I had a Gitlab page setup, with a pipeline, Lets Encrypt certificate and I was ready to rock. I reserved the domain years ago and now I finally had a reason to start using it.

The Gitlab setup provided everything and all seemed to work great. Sure I had to add my own image and link it correctly, make sure the transip CNAME was set up correctly, but it was all pretty easy. One thing however bothered me:
The dates of my posts were in the year 30300. Whatever I tried to change to the dateFormat that was provided at setup, it didn't change anything. Eventually I started editing the theme, however quickly realized this was not a way forward. I'm not a CSS or frontend developer, I can create presentations in revealjs, create network diagrams in Vizceral... But that's about it!

So I cloned the Beautiful Hugo theme, I hoped the gitlab version I had contained a bug and it was now fixed. The Beautiful Hugo theme update looked great... But the dates were still wrong. After reading the documentation from Hugo i found out that as of v0.19 of Hugo, the dateFormat was no longer supported. The new way to setup your date formatting is by adding this block in your config.toml

```text
[frontmatter]
date = ["date", "publishDate", "lastmod"]
lastmod = [":git", "lastmod", "date", "publishDate"]
publishDate = ["publishDate", "date"]
expiryDate = ["expiryDate"]
```

By updating your config.toml with these new parameters your date formatting is now correct.