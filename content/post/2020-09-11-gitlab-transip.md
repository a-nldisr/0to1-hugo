---
title: Gitlab pages hosting on TransIP with Lets Encrypt certificates
subtitle: Lets Encrypt It
date: 2020-09-11
tags:
- SSL
- TLS
- gitlab
- transip

---
After the 4th or 5th time the [https://0to1.nl](https://0to1.nl) domain hosting company got taken over, the interface seemed to get worse and worse, I decided to move the domain to TransIP. I worked with the TransIP interface before and it is easy to manage, they offer an API and 2FA so my requirements were quickly met.

Every couple of months I register a domain for a project I plan to work on. The end result is hundreds of euros in costs per year for domains I hardly use. Today was going to be different! I registered [https://imkerijsoest.nl](https://imkerijsoest.nl) for my beekeeping hobby and after struggling with setting up Let's Encrypt on TransIP I was reminded why I registered this domain: To document these steps and share them with the world. 

[https://0to1.nl](https://0to1.nl) is a Gitlab hosted Hugo blog and I wanted to set up my beekeeping domain in a similar setup.

Setting up the Hugo page is easy, you click to create a `New project` and then `select from Template`. What took me a couple of tries, however, was getting Lets Encrypt to perform DNS validation on my domain. First, you will have to make your first commit to the project, this will trigger the CI/CD pipeline and create the pages files for the Gitlab pages.

Gitlab pages can be used with a custom domain, in 2018 their IP changed as you can read in this post: [https://about.gitlab.com/blog/2018/08/28/gitlab-pages-update/](https://about.gitlab.com/blog/2018/08/28/gitlab-pages-update/)

We are going to point our domain to this IP by setting an A record. For simplicity's sake, we are going to set a wildcard also to this domain. While setting the DNS records up we can lower the TTL to 5 minutes, but once this is done we want to raise the TTL to 1 day. Having low TTL settings enables us to make changes fast but once we are done it's a good practice and nice for the DNS providers out there to set the TTL to higher.

While the Gitlab pages tell you to use CNAMES, TransIP does not allow you to use a CNAME for your domain.

(This CNAME directly to your domain does not work)

![CNAME](/img/gitlab-transip/cname.png)

So in this scenario, we use an A record.

![A Record](/img/gitlab-transip/a-record.png)

After you have created your Hugo project from the templates on GitLab, you can go to `Settings > Pages`. On this page you can click the option: `New Domain`. Force HTTPS at this stage does nothing.

![New Domain](/img/gitlab-transip/new-domain.png)

In the new screen, you need to set the domain and enable the Lets Encrypt usage.

![Set Domain](/img/gitlab-transip/set-domain.png)

After you press `Create New Domain` you land on a page where you see the warning that you need to verify it. First, we are going to add a domain to the Gitlab pages for  `www.mydomain.nl`, it always amazes me how many people still use www in front of the domain name but they do so don't forget to set it up. Not setting it would mean you lose this traffic.

![Verify Domain](/img/gitlab-transip/verify-domain.png)

On this page, there is some information that you cannot just copy and paste from Gitlab pages to TransIP. One of these is the CNAME they require you to set for the domain. TransIP simply does not allow this to be set. This is why we set the A record earlier.

![Incorrect Page](/img/gitlab-transip/www-pages.png)

Let's first setup the `www` part of the domain in Gitlab, the direct domain you can setup later by just leaving out the `www`. The only difference in this step is that can set up a CNAME to point to the `www` subdomain. Add the CNAME to the domain by using `www` in the left field, and place the CNAME on your GitLab page on the right. Gitlab offers to copy and paste, however, the TransIP interface does not allow to copy and paste in this format.

The way it looks in the TransIP interface: ![TransIP CNAME](/img/gitlab-transip/transip-cname.png)

Now use Lets Encrypt for the `www` CNAME you just configured. We need to add a DNS TXT record to perform DNS verification. Again the information on the Gitlab page cannot be used, TransIP simply does not allow the format, we have to adjust the format so they can perform a domain validation on the `www` part of our domain.

If you would copy the text as is below your verification will fail.

![Gitlab verification](/img/gitlab-transip/gitlab-www-verification.png)

Instead of using `_gitlab-pages-verification-code.www.imkerijsoest.nl` you need to use `_gitlab-pages-verification-code.www`. After some trial and error I found out this works, Gitlab can take a couple of days to perform this check so patience is required.

In TransIP it looks like this:

![TransIP verification](/img/gitlab-transip/transip-verification.png)

After all of this don't forget to Save your changes and now it is time to wait. Once the domain is validated and the Lets Encrypt certificate is ready, you can enforce `HTTPS` on the pages.

For the main domain we are going to use instead of `_gitlab-pages-verification-code.imkerijsoest.nl` this: `_gitlab-pages-verification-code`. We will simply drop the www part from the domain. In the right field you can copy over the verification code as you did before: `gitlab-pages-verification-code=my_long_verification_code_string`.

Now you have both www and the direct domain configured. It can take a couple of hours to days to get the Let's Encrypt domain. You just have to retry.

After I applied all these settings it took a couple of hours before the validation from Let's Encrypt passed. You have to manually trigger this validation every once in a while.