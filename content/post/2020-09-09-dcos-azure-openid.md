---
title: 'DCOS: Azure OpenID'
subtitle: With good documentation it should not be hard
date: 2020-09-09
tags:
- dcos
- openid
- azure

---
This HOWTO will very likely be out of date soon, since Microsoft updates their Azure portal way too often to make documentation reliable. The official documentation for setting up OpenID in Azure Active Directory was outdated and I could not find a few of the listed options they described on: [https://docs.microsoft.com/en-us/powerapps/maker/portals/configure/configure-openid-settings](https://docs.microsoft.com/en-us/powerapps/maker/portals/configure/configure-openid-settings). If this documentation can help one or two people with setting this up it would be worth it, since i spend a couple hours on finding the right endpoints and setup i decided to make this HOWTO. In this HOWTO we are going to set up OpenID for DCOS Enterprise. Currently this is all text, since I only have access to Azure through my employer and I do not want to share any details from their accounts. 

DCOS Enterprise offers support for two identity providers: SAML and OpenID.

## Azure portal

In the Azure portal type in the search bar: `Azure Active Directory`. This will lead you to the Azure Active Directory overview page. In the left `Manage` menu you can find the option: `App registrations`. In the next screen you will see the existing registered applications. In this page you have the `Endpoints` button, for some reason when I used the OpenID endpoint on this page the accounts created in DCOS enterprise were weird hashes. It looked like a MD5 sum or Base64 encoded string, but when I tried to decrypt it all I got was binary. So ignore these endpoints for OpenID setup, instead I looked through documentation. In the Microsoft documentation I found the [https://sts.windows.net](https://sts.windows.net) endpoint and by playing with that I eventually got it to work.

Click `New Registration`, this will open a new window with `Register an application` at the top. First enter a human readable name, in our case `DCOS` or `DCOS test`. After this you have a menu where you can select who can access your DCOS cluster, in our case we chose for: `Accounts in this organizational directory only (My_Company only - Single tenant)`. We want only those associated with our organisation to be able to login in our clusters. In the next field we are going to fill in the callback URI, Microsoft calls this the Redirect URI. From the menu select `Web` and fill in your Redirect URI: [https://dcos.myamazingdomain.com/acs/api/v1/auth/oidc/callback](https://dcos.myamazingdomain.com/acs/api/v1/auth/oidc/callback). After this we can register the Application.

Once this is done we can change some parameters in this new Application. You will land in the overview page in your new DCOS application. In this overview page you can verify and update the redirect endpoint you just set. We will however now add a logo and fill in the url of the service. In the left `Manage` menu you can click branding. In this page you can upload an icon, set the url to your DCOS cluster [https://dcos.myamazingdomain.com](https://dcos.myamazingdomain.com). After you have done this you can save these changes and let's move on to creating credentials. We will be doing this in the Credentials & secrets menu. When you open this menu you will see two options, for our DCOS setup we will create a new client secret. Click the New client secret, add a simple description like `DCOS OpenID secret` and fill in the lifetime of this secret. In this demo we chose `1 year`. After this press `Add`. Now you will see the new client secret, you can use this later in the DCOS interface.

Next step is that we are adding the `owners`, this is not a requirement to make OpenID work but depends on your organisation. After this you are done with Azure AD, you will need some details from this application so keep the tab open but save your changes and go to the overview page again.

## DCOS interface

Now we are ready to add the OpenID to the DCOS interface. In the DCOS interface click `Settings > Identity Providers`. Click the little `+` icon to add a provider. This will give you a selection menu where you can make a choice between `SAML` and `OpenID` Connect. Pick `OpenID Connect`.

* In the `provider` field place `microsoft-idp`.
* In the `Description` field you need to use a clear name. This name will be used for your DCOS users to offer as a login button.
* In the `Issuer` field we need to fill in the following: [https://sts.windows.net/your_tenant_ID](https://sts.windows.net/your_tenant_ID). You can find this `Directory (tenant) ID` in the Application overview page.
* In the `Base URI` field fill in the url from your DCOS cluster. We always use secure URIs, it could be that Microsoft rejects insecure URI's.
* In the `Client ID` field you can fill in the `Application (client) ID`, you can find this in the overview page from the application.
* Last field is the `Client Secret` field. Here we are going to place the secret we generated when we created the application.

When this is done you can save and the connection should be made. You can now login with OpenID in the login menu. This will give an `Access Denied error`, when you log out and log back in under your admin user you can now see the external user in the `Organization > Users` menu. Click the user and go to group membership. Add this user to the superusers group. Now you can login and work with OpenID.