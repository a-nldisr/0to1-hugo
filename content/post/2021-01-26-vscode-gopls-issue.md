---
tags:
- vscode
- " go"
- gopls
- golang
- langauge server
Title: 'VSCODE GOPLS: Language server slow'
Subtitle: Deprecated means trouble?
date: 2021-01-25T23:00:00+00:00

---
Today a short share.

Since last year I have been using golang more in my toolkit, often I copy paste but some moments I have to actually write code. For this I use vscode and gopls is the language server.

What is gopls? It stands for _Go please_. It is a language server and helps linting, autocompleting, adds warnings that this string is not of type \[\]some.thing. It integrates with my favorite IDE: vscode. Fancy stuff. And today was that day that it abandoned me and no longer responded. It did not autocomplete and didn't give warnings when I used a wrong type. The result is that every iteration I had to run go build to see if the compiler found any issues, the benefit for using a language server is that you get instant feedback. I was slowed down and I cannot have that.

After a quick search I found out that all the comments from the vscode developers on Github shared the same common question. `Can you open "Developer: Toggle Developer Tools" and provide the logs?`

Quickly I found out that this gives tons of information, tons of errors. I found out that some of the errors were caused by two extension: `tree-sitter` and the `Ansible` extension from Microsoft. Both were deprecated and seemed to cause issues with golang code. After disabling these extensions the issue was solved.

Sometimes it is that easy.

ps: I saw that the dates are no longer correctly displayed, have not had time to investigate it. Will fix soon.

Oh, the project I was building in go? Perhaps I can share that soon, it is pretty nice for those that are migrating from DCOS to Kubernetes.