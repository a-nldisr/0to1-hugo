---
title: Golang Hello world
subtitle: Using Golang code example
date: 2020-06-03
tags:
- example
- golang

---
```golang
package main

import "fmt"

func main() {
    fmt.Println("hello world")
}
```

Hello world!