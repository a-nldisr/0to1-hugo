---
tags:
- minikube
- rancher
- rancher-desktop
- docker
- kubernetes
- k8s
- docker-desktop
Title: Ranch your Minikube
Subtitle: Because why not?
date: 2022-01-07T23:00:00.000+00:00

---
In the previous post, we went through a lot of steps to get Minikube running on the Apple M1 Pro without `docker-desktop`. Eventually, it was working on a virtual machine with `vftool`. The problem with this setup is that it would take a lot of effort to get minikube accessible directly from my MacBook.

In the closing words, I already mentioned I had an idea to run Minikube directly on the M1 Pro. In a programming project, I needed an `etcd` and Kubernetes setup. I already used `rancher-desktop`, `rancher-desktop` lacked the `etcd` setup. The rancher-desktop setup is to run Kubernetes as minimal as possible and `etcd` is not included, so I had the idea to use Minikube and use its internal etcd cluster to connect my golang project against it.

Rancher desktop is from the creators of `k3s`, `rancher` itself, and other tools. You can use it to set up a minimal Kubernetes setup. By default, it comes with `nerdctl`, a `docker` compatible `cli`. It also offers to switch to `dockerd`.

As I mentioned in the other post: Docker changed its license and it is now a paid product under certain circumstances. This gave me the challenge: Can I run minikube without `docker-machine`? All other drivers for Minikube are not yet working on the M1 Pro. Only `vftool` and `docker` seem to be the two options to get minikube running on the M1 Pro.

## Using docker without docker-machine

In the previous post, I went into setting up `vftool`. In this post, I want to focus on the setup running with docker without `docker-desktop. `As a solution, I saw Rancher Desktop, which handles the installation for dockerd. 

First, install `rancher-desktop`. Since this product is still in alpha the way to set it up will likely change so please check the documentation at:

[https://rancherdesktop.io/](https://rancherdesktop.io/ "https://rancherdesktop.io/")

After installing you can run into the issue that the supporting utilities cannot be set up. You can find the solution here:  
[https://github.com/rancher-sandbox/rancher-desktop/issues/1155#issuecomment-1004831158](https://github.com/rancher-sandbox/rancher-desktop/issues/1155#issuecomment-1004831158 "https://github.com/rancher-sandbox/rancher-desktop/issues/1155#issuecomment-1004831158")

After this is done you can set up the utilities in Preferences.

![](/uploads/screenshot-2022-01-08-at-00-25-40.png)

Once this is done you can enable the `dockerd` container runtime.

![](/uploads/screenshot-2022-01-08-at-00-27-03.png)

After this, the virtual machine will restart and once this is done you can check if docker is running:

    docker ps

It will list all the containers running for `rancher-desktop`.

> CONTAINER ID   IMAGE                              COMMAND                  CREATED       STATUS       PORTS     NAMES
>
> 3c87bce21bd1   rancher/mirrored-metrics-server    "/metrics-server --c…"   2 hours ago   Up 2 hours             k8s_metrics-server_metrics-server-9cf544f65-d4cvl_kube-system_0a1fd6a5-a8f9-4a4c-8987-ec99066c0e5e_0
>
> a6aeb588a582   rancher/mirrored-library-traefik   "/entrypoint.sh --gl…"   2 hours ago   Up 2 hours             k8s_traefik_traefik-786ff64748-7jv49_kube-system_cf6f2619-8b1c-4e5d-a6d2-5a0a7cbd5f06_0
>
> d89ced005445   rancher/local-path-provisioner     "local-path-provisio…"   2 hours ago   Up 2 hours             k8s_local-path-provisioner_local-path-provisioner-64ffb68fd-m4rqm_kube-system_d9696a2c-7fd4-4dde-9649-1f89c152b56e_0
>
> 004f5a4213b3   625882d9991e                       "entry"                  2 hours ago   Up 2 hours             k8s_lb-port-443_svclb-traefik-75lm5_kube-system_64e24be8-b92e-42a4-9eb3-1f6678a801fc_0
>
> 71c7254bae4c   rancher/klipper-lb                 "entry"                  2 hours ago   Up 2 hours             k8s_lb-port-80_svclb-traefik-75lm5_kube-system_64e24be8-b92e-42a4-9eb3-1f6678a801fc_0
>
> 8778667090df   rancher/mirrored-coredns-coredns   "/coredns -conf /etc…"   2 hours ago   Up 2 hours             k8s_coredns_coredns-85cb69466-29gsc_kube-system_45ad3423-2428-479c-9edb-08684428ed40_0
>
> b10897a6a715   rancher/mirrored-pause:3.1         "/pause"                 2 hours ago   Up 2 hours             k8s_POD_metrics-server-9cf544f65-d4cvl_kube-system_0a1fd6a5-a8f9-4a4c-8987-ec99066c0e5e_0
>
> d948d24f5610   rancher/mirrored-pause:3.1         "/pause"                 2 hours ago   Up 2 hours             k8s_POD_traefik-786ff64748-7jv49_kube-system_cf6f2619-8b1c-4e5d-a6d2-5a0a7cbd5f06_0
>
> 4629cc122fa5   rancher/mirrored-pause:3.1         "/pause"                 2 hours ago   Up 2 hours             k8s_POD_local-path-provisioner-64ffb68fd-m4rqm_kube-system_d9696a2c-7fd4-4dde-9649-1f89c152b56e_0
>
> 4c7ff0701363   rancher/mirrored-pause:3.1         "/pause"                 2 hours ago   Up 2 hours             k8s_POD_coredns-85cb69466-29gsc_kube-system_45ad3423-2428-479c-9edb-08684428ed40_0
>
> 9350f783ffac   rancher/mirrored-pause:3.1         "/pause"                 2 hours ago   Up 2 hours             k8s_POD_svclb-traefik-75lm5_kube-system_64e24be8-b92e-42a4-9eb3-1f6678a801fc_0

I was still under the impression that Minikube had a full working and accessible etcd cluster, after this setup I found out that this was not the case. But the only way I could get Minikube running at this time was using Rancher Desktop. So how I got Minikube working was using the docker driver I got setup with Rancher Desktop:

    minikube start --driver=docker

> 😄  minikube v1.24.0 on Darwin 12.1 (arm64)
>
> ✨  Using the docker driver based on user configuration
>
> 👍  Starting control plane node minikube in cluster minikube
>
> 🚜  Pulling base image ...
>
> 🔥  Creating docker container (CPUs=2, Memory=5880MB) ...
>
>     > kubeadm.sha256: 64 B / 64 B \[--------------------------\] 100.00% ? p/s 0s
>     
>     > kubectl.sha256: 64 B / 64 B \[--------------------------\] 100.00% ? p/s 0s
>     
>     > kubelet.sha256: 64 B / 64 B \[--------------------------\] 100.00% ? p/s 0s
>     
>     > kubeadm: 40.50 MiB / 40.50 MiB \[-------------\] 100.00% 15.85 MiB p/s 2.8s
>     
>     > kubectl: 41.44 MiB / 41.44 MiB \[-------------\] 100.00% 10.79 MiB p/s 4.0s
>     
>     > kubelet: 107.26 MiB / 107.26 MiB \[-----------\] 100.00% 16.00 MiB p/s 6.9s
>     
>     ▪ Generating certificates and keys ...
>     
>     ▪ Booting up control plane ...
>     
>     ▪ Configuring RBAC rules ...
>
> 🔎  Verifying Kubernetes components...
>
>     ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
>
> 🌟  Enabled addons: storage-provisioner, default-storageclass
>
> 🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default

This will start the minikube cluster and you can start using it:

    minikube kubectl -- get pods -A

> NAMESPACE     NAME                               READY   STATUS    RESTARTS      AGE
>
> kube-system   coredns-78fcd69978-bnmzv           1/1     Running   0             33m
>
> kube-system   etcd-minikube                      1/1     Running   0             33m
>
> kube-system   kube-apiserver-minikube            1/1     Running   0             33m
>
> kube-system   kube-controller-manager-minikube   1/1     Running   0             33m
>
> kube-system   kube-proxy-rk79l                   1/1     Running   0             33m
>
> kube-system   kube-scheduler-minikube            1/1     Running   0             33m
>
> kube-system   storage-provisioner                1/1     Running   1 (33m ago)   33m

## The difference with vftool

This setup allows direct access on the localhost interface to all the minikube components, unfortunately its etcd setup is configured to not bind to these interfaces. Another benefit compared to vftool is when I restart the laptop I also can quickly get this setup up and running, with `vftool` I needed to spend a lot more effort to get persistence and had to setup Ubuntu every time whereas with Minikube the docker containers just get resumed. I had the impression that Minikube came with an etcd cluster accessible, however unfortunately this was not the case so Minikubes use case was no longer there. See my other posts about this. Lesson learned: If you need an etcd cluster locally on the M1 pro, just skip Minikube and install it directly through Rancher Desktop. 