---
tags:
- keybase
- m1
- apple
- FUSE
Title: Keybase broken on the M1 Pro
Subtitle: 'Scripting your way out of trouble. '
date: 2022-01-25T23:00:00+00:00

---
Recently I needed to get access to Keybase since I bought the M1 Pro. During the first try to sync the folders in a Keybase team the application hung and refused to copy the secrets to the filesystem. Keybase is allowed to download these secrets, however copying dozens of secrets by hand 1 by 1 is not an option.

After some investigation, I found out that Keybase relies on FUSE to mount the filesystem. According to [this](https://github.com/keybase/client/issues/24636#issuecomment-981931397) topic, it happened more often.

Waiting for a fix was not an option and trying to fix the integration myself failed, I simply had no knowledge of how Keybase internals work to commit a solution to the project in a short term. So I resorted to my usual approach when I am in trouble: Script my way out of hell.

The result is [this](https://gist.github.com/a-nldisr/176f5da07187f827b6734edf2b62ca7a) little gist, feel free to use it! I hope it helps others in this situation.

Things I noticed while using Keybase on the commandline:

* The application has a lot of stability issues. I decided to make it fail fast so you can safely run it again.
* The copy command will not copy over existing keys, it will instead create a new nested folder underneath and secrets there. The result is that you now have 2 secrets on disk, making it difficult to understand which is which if you return to that folder in 6 months' time. So I decided to skip the copy action if a secret is already there.

## Future of Keybase?

Zoom acquired Keybase in 2021, they pulled development capacity from Keybase which is clearly visible in the GitHub repository. Development efforts in the client slowed down in 2021. Currently, I would not start a new project based on Keybase.

![commit](/uploads/screenshot-2022-01-26-at-19-53-23.png "Keybase")

Keybase offered many great features that were certainly interesting, its ability to encrypt your git repositories and team-based sharing of secrets are very nice features that the community will surely miss.